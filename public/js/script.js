// //Questão 1
$(document).ready(function () {
    
    var form = $('#form_1');
    var a = form.find("#a");
    var b = form.find("#b");
    var c = form.find("#c");
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    var btn = form.find("#btn");

    var divRes = $('#resposta1');

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': CSRF_TOKEN
        }
    });    

    btn.on('click', function (e) {
        e.preventDefault();

        divRes.html('');

        var data = {
            a: a.val(),
            b: b.val(),
            c: c.val(),
        }

        $.ajax({
            type: "post",
            url: "/resposta1",
            data: {
                data:data
            },
            dataType: "json",
            success: function (data) {
                console.log(data);
                var delta = data.delta;
                var x1 = data.x1;
                var x2 = data.x2;

                var res = '<h4>Resposta</h4>'
                        +  '<p>'+ delta +'</p>';

                if (typeof x1 != "undefined" || x1 != null){
                    res += "<p> x'= " + x1 + "</p>";
                }
                if (typeof x2 != "undefined" || x2 != null){
                    res += "<p> x''= " + x2 + "</p>";
                }

                divRes.append(res);      
                
            },
            error: function (data) {
                console.log('Error:', data);
            }
        });
        
    });
});
