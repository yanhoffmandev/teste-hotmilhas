<?php

namespace Estacionamento;

class Veiculo
{

    private $modelo;
    private $placa;

    public function __construct($modelo, $placa)
    {

        $this->modelo = $modelo;
        $this->placa = $placa;
    }

    public function getModelo()
    {
        return $this->modelo;
    }

    public function setModelo($modelo)
    {
        $this->modelo = $modelo;

        return $this;
    }

    public function getPlaca()
    {
        return $this->placa;
    }

    public function setPlaca($placa)
    {
        $this->placa = $placa;

        return $this;
    }
}
