<?php

namespace Estacionamento;

class Estacionamento
{

    private $cliente; // classe
    private $veiculo; // classe
    private $valorHora;
    private $tipoCadastro;
    private $horaEntrada;
    private $horaSaida;

    public function __construct(Cliente $cliente, Veiculo $veiculo)
    {
        $this->cliente = $cliente;
        $this->veiculo = $veiculo;
        $this->valorHora = 7;
    }

    public function entrada($hora)
    {
        $this->setHoraEntrada($hora);
    }

    public function saida($hora)
    {
        $this->setHoraSaida($hora);
    }

    private function tempoPermanencia()
    {
        $entrada = $this->getHoraEntrada();
        $saida = $this->getHoraSaida();
        $hora1 = explode(":", $entrada);
        $hora2 = explode(":", $saida);
        $acumulador1 = ($hora1[0] * 3600) + ($hora1[1] * 60);
        $acumulador2 = ($hora2[0] * 3600) + ($hora2[1] * 60); 
        $resultado = $acumulador2 - $acumulador1;
        $hora_ponto = floor($resultado / 3600);
        $resultado = $resultado - ($hora_ponto * 3600);
        $min_ponto = floor($resultado / 60);
        //Grava na variável resultado final
        return $hora_ponto . ":" . $min_ponto;
    }

    public function totalPagar()
    {
        //os minutos conta como uma hora de estadia
        //Ex.: 6:01 = 7 
        $tempo = $this->tempoPermanencia();
        $hora = explode(":", $tempo);
        $hora = (intval($hora[1]) > 0) ? $hora[0] + 1 : $hora[0];
        return $hora * $this->getValorHora();
    }

    public function getCliente()
    {
        return $this->cliente;
    }

    public function setCliente($cliente)
    {
        $this->cliente = $cliente;

        return $this;
    }

    public function getVeiculo()
    {
        return $this->veiculo;
    }

    public function setVeiculo($veiculo)
    {
        $this->veiculo = $veiculo;

        return $this;
    }

    public function getValorHora()
    {
        return $this->valorHora;
    }

    public function setValorHora($valorHora)
    {
        $this->valorHora = $valorHora;

        return $this;
    }

    public function getHoraEntrada()
    {
        return $this->horaEntrada;
    }

    public function setHoraEntrada($horaEntrada)
    {
        $this->horaEntrada = $horaEntrada;

        return $this;
    }

    public function getHoraSaida()
    {
        return $this->horaSaida;
    }

    public function setHoraSaida($horaSaida)
    {
        $this->horaSaida = $horaSaida;

        return $this;
    }

}
