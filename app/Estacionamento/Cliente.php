<?php

namespace Estacionamento;

class Cliente
{

    private $id;
    private $nome;
    private $cpf;

    
    public function __construct($nome, $cpf){
        $this->id = rand(11111,99999);
        $this->nome = $nome;
        $this->cpf = $cpf;
    }

    public function getId()
    {
        return $this->id;
    }


    public function getNome()
    {
        return $this->nome;
    }

    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    public function getCpf()
    {
        return $this->cpf;
    }

    public function setCpf($cpf)
    {
        $this->cpf = $cpf;

        return $this;
    }
}
