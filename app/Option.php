<?php

namespace App;

use App\Survey;
use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    public function survey()
    {
        return $this->belongsTo(Survey::class, 'survey_id');
    }
}
