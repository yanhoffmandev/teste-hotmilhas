<?php

namespace App;

use App\Option;
use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    protected $fillable = [
        'pergunta'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function options()
    {
        return $this->hasMany(Option::class);
    }
}
