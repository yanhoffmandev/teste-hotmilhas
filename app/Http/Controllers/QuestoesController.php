<?php

namespace App\Http\Controllers;

use App\Option;
use App\Survey;
use Estacionamento\Cliente;
use Estacionamento\Estacionamento;
use Estacionamento\Veiculo;
use Illuminate\Http\Request;

class QuestoesController extends Controller
{
    public function questao1(Request $request)
    {
        $tituloGuia = 'Questão 1';
        return view('questoes.questao1', compact('tituloGuia'));
    }

    public function resposta1(Request $request)
    {
        $dados = $request->get('data');

        $a = $dados['a'];
        $b = $dados['b'];
        $c = $dados['c'];

        $delta = ($b * $b) - 4 * $a * $c;
        switch (true) {
            case ($delta < 0):
                $result['delta'] = "Delta negativo, não exitem raízes Reais.";
                break;
            case ($delta === 0):
                // Delta = 0, raizes iguais
                $result["x1"] = number_format((-$b + sqrt($delta)) / (2 * $a), 2, ',', '.');
                $result["x2"] = number_format((-$b + sqrt($delta)) / (2 * $a), 2, ',', '.');

                $result["delta"] = "Delta = 0, raízes iguais.";
                break;
            default:
                // Delta positivo. Raizes diferentes
                $result["x1"] = number_format((-$b + sqrt($delta)) / (2 * $a), 2, ',', '.');
                $result["x2"] = number_format((-$b - sqrt($delta)) / (2 * $a), 2, ',', '.');

                $result["delta"] = "Delta positivo, raízes diferentes.";
                break;
        }

        // return view('questoes.questao1',compact('result'));

        return response()->json($result);
    }

    //Questão 2
    public function questao2(Request $request)
    {
        $tituloGuia = 'Questão 2';
        return view('questoes.questao2', compact('tituloGuia'));
    }
    //Questão 3
    public function questao3(Request $request)
    {

        $cliente = new Cliente('Carlos Almeida', '083641235-06');
        $carro = new Veiculo('Uno', 'hej-2343');
        $estacionamento = new Estacionamento($cliente, $carro);

        $estac['cliente'] = $estacionamento->getCliente();
        $estac['veiculo'] = $estacionamento->getVeiculo();

        $estacionamento->entrada('14:01');
        $estacionamento->saida('20:00');
        $estacionamento->totalPagar();

        $estac['estacionamento'] = $estacionamento;

        $tituloGuia = 'Questão 3';

        return view('questoes.questao3', compact('estac', 'tituloGuia'));
    }
    //Questão 4
    public function questao4(Request $request)
    {
        $tituloGuia = 'Questão 4';
        return view('questoes.questao4');
    }
    //Questão 5
    public function questao5(Request $request)
    {
        $tituloGuia = 'Questão 5';
        return view('questoes.questao5', compact('tituloGuia'));
    }
    //Questão 6
    public function questao6()
    {
        $tituloGuia = 'Questão 6';
        return view('questoes.questao6', compact('tituloGuia'));
    }

    public function resposta6(Request $request)
    {
        $m = [[]];
        $x = rand(1, 9);
        for ($l = 0, $n = 3; $l < $n; $l++, $x++) {
            for ($c = 0; $c < $n; $c++, $x += $n) {
                for ($k = 0; $k < $n * $n; $k++, $x++) {
                    $m[$n * $l + $c][$k] = ($x % ($n * $n)) + 1;
                }
            }
        }

        return view('questoes.questao6', compact('m'));
    }
    //Questão 7
    public function questao7()
    {
        $enquetes = Survey::all();

        $tituloGuia = 'Questão 7';

        return view('questoes.questao7', compact('enquetes', 'tituloGuia'));
    }

    public function submitVoto(Request $request)
    {
        $dados = $request->all();
        $opcaoVotada = Option::find($dados['voto']);
        $opcaoVotada->votos = $opcaoVotada->votos + 1;
        $opcaoVotada->update();
        \Session::flash('mensagem', ['msg' => 'Voto computado!', 'class' => 'green white-text']);

        return redirect()->back();
    }

    //Questão 8
    public function questao8()
    {
        $tituloGuia = 'Questão 8';
        return view('questoes.questao8', compact('tituloGuia'));
    }
}
