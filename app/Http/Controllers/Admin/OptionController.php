<?php

namespace App\Http\Controllers\Admin;

use App\Option;

use App\Survey;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OptionController extends Controller
{
    public function index($id)
    {
        $enquete = Survey::find($id);

        $registros = $enquete->options;        

        return view('admin.options.index', compact('registros', 'enquete'));
    }

    public function adicionar($id)
    {
        $enquete = Survey::find($id);
        return view('admin.options.adicionar', compact('enquete'));
    }
    public function salvar(Request $request, $id)
    {
        $enquete = Survey::find($id);
        $registro = new Option();
        $dados = $request->all();
        $registro->votos = 0;
        $registro->nome = $dados['nome'];
        $registro->survey_id = $enquete->id;        
    
        $registro->save();
        \Session::flash('mensagem', ['msg' => 'Opção cadastrada com sucesso!', 'class' => 'green white-text']);

        return redirect()->route('admin.options',$enquete->id);

    }
    public function editar($id)
    {
        $registro = Option::find($id);
        $survey = $registro->surveys;
        return view('admin.options.editar', compact('survey', 'registro'));

    }
    public function atualizar(Request $request, $id)
    {
        $registro = Option::find($id);
        $dados = $request->all();
        $registro->nome = $dados['nome'];

        $survey = $registro->survey;        

        $registro->update();
        \Session::flash('mensagem', ['msg' => 'Opção atualizada com sucesso!', 'class' => 'green white-text']);

        return redirect()->route('admin.options', $survey->id);

    }
    public function deletar($id)
    {   
        $option = Option::find($id);
        $survey = $option->survey;

        $option->delete();
        
        \Session::flash('mensagem', ['msg' => 'Opção deletada com sucesso!', 'class' => 'green white-text']);

        return redirect()->route('admin.options', $survey->id);

    }
}
