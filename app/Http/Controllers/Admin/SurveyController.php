<?php

namespace App\Http\Controllers\Admin;

use App\Survey;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SurveyController extends Controller
{
    public function index()
    {   
        $usuario = auth()->user();
        $registros = $usuario->surveys;
        
        return view('admin.enquetes.index', compact('registros','usuario'));
    }

    public function adicionar()
    {
        
        return view('admin.enquetes.adicionar');
    }

    public function salvar(Request $request)
    {
        $dados = $request->all(); 
        $enquete = new Survey();
        $enquete->pergunta = $dados['pergunta'];
        $enquete->user_id = auth()->user()->id;
        $enquete->save();
        \Session::flash('mensagem', ['msg' => 'Enquente cadastrada com sucesso!', 'class' => 'green white-text']);

        return redirect()->route('admin.enquetes');
    }

    public function editar(Request $request, $id)
    {

        $registro = Survey::find($id);
        return view('admin.enquetes.editar', compact('registro'));
    }

    public function atualizar(Request $request, $id)
    {

        Survey::find($id)->update($request->all());
        \Session::flash('mensagem', ['msg' => 'Enquente atualizada com sucesso!', 'class' => 'green white-text']);

        return redirect()->route('admin.enquetes');

    }

    public function deletar($id)
    {
    
        Survey::find($id)->delete();
        \Session::flash('mensagem', ['msg' => 'Enquente deletada com sucesso!', 'class' => 'green white-text']);

        return redirect()->route('admin.enquetes');

    }
}
