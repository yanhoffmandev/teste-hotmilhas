<?php

use App\User;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
 */

Route::get('/', ['as' => 'home', function () {
    return view('home');
}]);

Route::get('/questao1', ['as' => 'questoes.questao1', 'uses' =>
    'QuestoesController@questao1']);

Route::post('/resposta1', ['as' => 'questoes.resposta1', 'uses' =>
    'QuestoesController@resposta1']);

Route::get('/questao2', ['as' => 'questoes.questao2', 'uses' =>
    'QuestoesController@questao2']);

Route::get('/questao3', ['as' => 'questoes.questao3', 'uses' =>
    'QuestoesController@questao3']);

Route::get('/questao4', ['as' => 'questoes.questao4', 'uses' =>
    'QuestoesController@questao4']);

Route::get('/questao5', ['as' => 'questoes.questao5', 'uses' =>
    'QuestoesController@questao5']);

Route::get('/questao6', ['as' => 'questoes.questao6', 'uses' =>
    'QuestoesController@questao6']);

Route::post('/resposta6', ['as' => 'questoes.resposta6', 'uses' =>
    'QuestoesController@resposta6']);

Route::get('/questao7', ['as' => 'questoes.questao7', 'uses' =>
    'QuestoesController@questao7']);

Route::post('/voto', ['as' => 'questoes.voto', 'uses' =>
    'QuestoesController@submitVoto']);

Route::get('/questao8', ['as' => 'questoes.questao8', 'uses' =>
    'QuestoesController@questao8']);

//LOGIN
Route::get('/admin/login', ['as' => 'admin.login', function () {
    $users =  User::all();
    return view('admin.login.index', compact('users'));
}]);
Route::post('/admin/login', ['as' => 'admin.login', 'uses' =>
    'Admin\UserController@login']);

//ADMIN
Route::group(['middleware' => 'auth'], function () {

    //Login
    Route::get('/admin/login/sair', ['as' => 'admin.login.sair', 'uses' =>
        'Admin\UserController@sair']);

    //USUÁRIO
    //LISTAR
    Route::get('/admin/usuarios', ['as' => 'admin.usuarios', 'uses' =>
        'Admin\UserController@index']);
    //ADICIONAR
    Route::get('/admin/usuarios/adicionar', ['as' => 'admin.usuarios.adicionar', 'uses' =>
        'Admin\UserController@adicionar']);

    Route::post('/admin/usuarios/salvar', ['as' => 'admin.usuarios.salvar', 'uses' =>
        'Admin\UserController@salvar']);

    //ATUALIZAR
    Route::get('/admin/usuarios/editar/{id}', ['as' => 'admin.usuarios.editar', 'uses' =>
        'Admin\UserController@editar']);

    Route::put('/admin/usuarios/atualizar/{id}', ['as' => 'admin.usuarios.atualizar', 'uses' =>
        'Admin\UserController@atualizar']);

    //DELETAR
    Route::get('/admin/usuarios/deletar/{id}', ['as' => 'admin.usuarios.deletar', 'uses' =>
        'Admin\UserController@deletar']);


    //SURVEYS
    //LISTAR
    Route::get('/admin/enquetes', ['as' => 'admin.enquetes', 'uses' =>
        'Admin\SurveyController@index']);
    //ADICIONAR
    Route::get('/admin/enquetes/adicionar/', ['as' => 'admin.enquetes.adicionar', 'uses' =>
        'Admin\SurveyController@adicionar']);

    Route::post('/admin/enquetes/salvar/', ['as' => 'admin.enquetes.salvar', 'uses' =>
        'Admin\SurveyController@salvar']);

    //ATUALIZAR
    Route::get('/admin/enquetes/editar/{id}', ['as' => 'admin.enquetes.editar', 'uses' =>
        'Admin\SurveyController@editar']);

    Route::put('/admin/enquetes/atualizar/{id}', ['as' => 'admin.enquetes.atualizar', 'uses' =>
        'Admin\SurveyController@atualizar']);

    //DELETAR
    Route::get('/admin/enquetes/deletar/{id}', ['as' => 'admin.enquetes.deletar', 'uses' =>
        'Admin\SurveyController@deletar']);

    //OPTIONS
    //LISTAR
    Route::get('/admin/options/{id}', ['as' => 'admin.options', 'uses' =>
        'Admin\OptionController@index']);
    //ADICIONAR
    Route::get('/admin/options/adicionar/{id}', ['as' => 'admin.options.adicionar', 'uses' =>
        'Admin\OptionController@adicionar']);

    Route::post('/admin/options/salvar/{id}', ['as' => 'admin.options.salvar', 'uses' =>
        'Admin\OptionController@salvar']);

    //ATUALIZAR
    Route::get('/admin/options/editar/{id}', ['as' => 'admin.options.editar', 'uses' =>
        'Admin\OptionController@editar']);

    Route::put('/admin/options/atualizar/{id}', ['as' => 'admin.options.atualizar', 'uses' =>
        'Admin\OptionController@atualizar']);

    //DELETAR
    Route::get('/admin/options/deletar/{id}', ['as' => 'admin.options.deletar', 'uses' =>
        'Admin\OptionController@deletar']);


});
