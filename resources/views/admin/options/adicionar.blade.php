@extends('app')

@section('content')
<div class="container">
    <h2 class="center">Adicionar Opção</h2>

    <div class="row">
        <nav>
            <div class="nav-wrapper orange">
                <div class="col s12">
                    <a href="{{ route('admin.usuarios') }} " class="breadcrumb breadhover">Início</a>
                    <a href="{{ route('admin.enquetes') }}" class="breadcrumb breadhover">Lista de Enquetes</a>
                    <a href="{{ route('admin.options',$enquete->id) }}" class="breadcrumb breadhover">Lista de Opções</a>
                    <a class="breadcrumb">Editar Opção</a>
                </div>
            </div>
        </nav>
    </div>

    <div class="row">
        <form action="{{ route('admin.options.salvar', $enquete->id) }}" method="POST">
            {{ csrf_field() }}
            @include('admin.options._form')
            <button class="btn blue waves-effect waves-light">Adicionar</button>
        </form>
    </div>

</div>
@endsection