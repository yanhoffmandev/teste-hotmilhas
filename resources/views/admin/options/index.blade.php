@extends('app')

@section('content')
<div class="container">
    <h2 class="center">Lista de Opções</h2>

    <div class="row">
        <nav>
            <div class="nav-wrapper orange">
                <div class="col s12">
                    <a href="{{ route('admin.usuarios') }} " class="breadcrumb breadhover">Início</a>
                    <a href="{{ route('admin.enquetes') }} " class="breadcrumb breadhover">Lista de Enquetes</a>
                    <a class="breadcrumb">Lista de Opções</a>
                </div>
            </div>
        </nav>
    </div>

    <div class="row">
        <table>
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Nome</th>
                    <th>Votos</th>
                    <th>Ação</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($registros as $registro)
                    <tr>
                        <td>{{ $registro->id }}</td>
                        <td>{{ $registro->nome }}</td>
                        <td>{{ $registro->votos }}</td>
                        <td> 
                            <a href="{{ route('admin.options.editar',$registro->id) }}" class="btn orange waves-effect waves-light">Editar</a>
                            <a href="javascript: if(confirm('Deletar Opção?')){ window.location.href='{{ route('admin.options.deletar',$registro->id) }}'}" class="btn red waves-effect waves-light">Deletar</a>                     
                        </td>
                    </tr>
                @endforeach        
            </tbody>
        </table>
    </div>
    <div class="row">
        <a href="{{ route('admin.options.adicionar',$enquete->id) }}" class="btn blue waves-effect waves-light">Adicionar</a>
    </div>
</div>   
@endsection
