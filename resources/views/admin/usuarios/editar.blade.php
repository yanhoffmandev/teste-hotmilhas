@extends('app')

@section('content')
<div class="container">
    <h2 class="center">Editar Usuário</h2>

    <div class="row">
        <nav>
            <div class="nav-wrapper orange">
                <div class="col s12">
                    <a href="{{ route('admin.usuarios') }} " class="breadcrumb breadhover">Início</a>
                    <a href="{{ route('admin.usuarios') }}" class="breadcrumb breadhover">Lista de Usuários</a>
                    <a class="breadcrumb">Editar Usuário</a>
                </div>
            </div>
        </nav>
    </div>

    <div class="row">
        <form action="{{ route('admin.usuarios.atualizar', $usuario->id) }}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="put">
            @include('admin.usuarios._form')
            <button class="btn blue waves-effect waves-light">Adicionar</button>
        </form>
    </div>

</div>
@endsection