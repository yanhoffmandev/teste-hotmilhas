@extends('app')

@section('content')

<div class="container">
    <div class="row">
        <h2>Entrar</h2>
        <form action="{{ route('admin.login') }} " method="post">
            {{ csrf_field() }}

            @include('admin.login._form')

            <button class="btn blue waves-effect waves-light">Entrar</button>
        </form>
    </div>
    <div class="row">
        
        @foreach ($users as $user)
        <div class="col s12 m3">
            <p>Email:{{$user->email}}</p>
            <p>Senha:123456</p>
        </div>
        @endforeach 
    </div>
</div>

    
@endsection

