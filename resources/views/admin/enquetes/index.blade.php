@extends('app')

@section('content')
<div class="container">
    <h2 class="center">Lista de Enquetes</h2>

    <div class="row">
        <nav>
            <div class="nav-wrapper orange">
                <div class="col s12">
                    <a href="{{ route('admin.usuarios') }} " class="breadcrumb breadhover">Início</a>
                    <a class="breadcrumb">Lista de Enquetes</a>
                </div>
            </div>
        </nav>
    </div>

    <div class="row">
        <table>
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Pergunta</th>
                    <th>Ação</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($registros as $registro)
                    <tr>
                        <td>{{ $registro->id }}</td>
                        <td>{{ $registro->pergunta }}</td>
                        <td> 
                            <a href="{{ route('admin.enquetes.editar',$registro->id) }}" class="btn orange waves-effect waves-light">Editar</a>
                            <a href="{{ route('admin.options',$registro->id) }}" class="btn blue waves-effect waves-light">Opções</a>
                            <a href="javascript: if(confirm('Deletar enquete?')){ window.location.href='{{ route('admin.enquetes.deletar',$registro->id) }}'}" class="btn red waves-effect waves-light">Deletar</a>                     
                        </td>
                    </tr>
                @endforeach        
            </tbody>
        </table>
    </div>
    <div class="row">
        <a href="{{ route('admin.enquetes.adicionar') }}" class="btn blue waves-effect waves-light">Adicionar</a>
    </div>
</div>   
@endsection
