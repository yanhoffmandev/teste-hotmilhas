@extends('app')

@section('content')
<div class="container">
    <h2 class="center">Editar Enquete</h2>

    <div class="row">
        <nav>
            <div class="nav-wrapper orange">
                <div class="col s12">
                    <a href="{{ route('admin.usuarios') }} " class="breadcrumb breadhover">Início</a>
                    <a href="{{ route('admin.enquetes') }}" class="breadcrumb breadhover">Lista de Enquetes</a>
                    <a class="breadcrumb">Editar Enquete</a>
                </div>
            </div>
        </nav>
    </div>

    <div class="row">
        <form action="{{ route('admin.enquetes.atualizar', $registro->id) }}" method="POST">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="put">
            @include('admin.enquetes._form')
            <button class="btn blue waves-effect waves-light">Atualizar</button>
        </form>
    </div>

</div>
@endsection