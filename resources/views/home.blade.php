@extends('app')

@section('content')
    <div class="container">
    	<h2 class="center">Questionário</h2>
	<div class="row">
        <div class="col s12 m3">
            <div class="card green darken-1 waves-effect waves-light">
                <div class="card-content white-text">
                    <span class="card-title">Questão 1</span>
                </div>
                <div id="q1" class="card-action">
                <a  class="white-text" href="{{ route('questoes.questao1')}}">Ver Resposta</a>
                </div>
            </div>
        </div>
        <div class="col s12 m3">
            <div class="card blue darken-1 waves-effect waves-light">
                <div class="card-content white-text">
                    <span class="card-title">Questão 2</span>
                </div>
                <div id="q2" class="card-action">
                    <a class="white-text" href="{{ route('questoes.questao2')}}" >Ver Resposta</a>
                </div>
            </div>
        </div>
        <div class="col s12 m3">
            <div class="card deep-purple darken-1 waves-effect waves-light">
                <div class="card-content white-text">
                    <span class="card-title">Questão 3</span>
                </div>
                <div id="q3" class="card-action">
                    <a class="white-text" href="{{ route('questoes.questao3')}}">Ver Resposta</a>
                </div>
            </div>
        </div>
        <div class="col s12 m3">
            <div class="card orange darken-1 waves-effect waves-light">
                <div class="card-content white-text">
                    <span class="card-title">Questão 4</span>
                </div>
                <div id="q4" class="card-action">
                    <a class="white-text" href="{{ route('questoes.questao4')}}" >Ver Resposta</a>
                </div>
            </div>
        </div>
        
        
	</div>
	<div class="row">
        <div class="col s12 m3">
            <div class="card deep-orange darken-1 waves-effect waves-light">
                <div class="card-content white-text">
                    <span class="card-title">Questão 5</span>
                </div>
                <div id="q5"class="card-action">
                    <a class="white-text" href="{{ route('questoes.questao5')}}" >Ver Resposta</a>
                </div>
            </div>
        </div>
        <div class="col s12 m3">
            <div class="card red darken-1 waves-effect waves-light">
                <div class="card-content white-text">
                    <span class="card-title">Questão 6</span>
                </div>
                <div id="q6"class="card-action">
                    <a class="white-text" href="{{ route('questoes.questao6')}}">Ver Resposta</a>
                </div>
            </div>
        </div>
        <div class="col s12 m3">
            <div class="card  pink darken-4 waves-effect waves-light">
                <div class="card-content white-text">
                    <span class="card-title">Questão 7</span>
                </div>
                <div id="q7" class="card-action">
                    <a class="white-text" href="{{ route('questoes.questao7')}}">Ver Resposta</a>
                </div>
            </div>
        </div>
        
	
		<div class="col s12 m3">
            <div class="card indigo darken-4 waves-effect waves-light">
                <div class="card-content white-text">
                    <span class="card-title">Questão 8</span>
                </div>
                <div id="q8" class="card-action">
                    <a class="white-text" href="{{ route('questoes.questao8')}}">Ver Resposta</a>
                </div>
            </div>
        </div>
    
</div>


@endsection