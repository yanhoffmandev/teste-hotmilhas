<!DOCTYPE html>
<html lang="pt">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title> {{ isset($tituloGuia) ? $tituloGuia : "Hotmilhas Teste" }}</title>

    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
    <link rel="stylesheet" text="text/css" href="{{asset('css/style.css')}}">

</head>
<body id="app-layout">
    
    <header>
        @if(Auth::guest())
            @include('layouts._nav')
        @else
            @include('layouts._navadmin')    
        @endif
    </header>

    <main>
        @if (Session::has('mensagem'))
            <div class="container">
                <div class="row">
                    <div class="card {{ Session::get('mensagem')['class'] }}">
                        <div class="card-content" align="center">
                            {{ Session::get('mensagem')['msg'] }}
                        </div>
                    </div>
                </div>
            </div>
        @endif
    @yield('content')
    </main>


    <footer >
        {{-- @include('layouts._footer') --}}
    </footer>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
    <script src="{{asset('js/script.js')}}"></script>
</body>
</html>
