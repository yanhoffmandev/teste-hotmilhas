@extends('app')

@section('content')
<div class="container">
    <h2 class="center">Questão 5</h2>
    <div class="row">
        <nav>
            <div class="nav-wrapper orange">
                <div class="col s12">
                <a href="{{route('home')}}" class="breadcrumb breadhover">Início</a>
                    <a class="breadcrumb">Questão 5</a>
                </div>
            </div>
        </nav>
    </div>

    <div class="row">
            <h5 class="titulo">
                Em NodeJS existem threads?
            </h5>
        <div class="row">
            <div id="resposta5">
                <h4>Resposta</h4>
                <p>O NodeJS não tem threads como rescurso nativo. Mas se for preciso trabalhar com threads há plugins externos.</p>
            </div>
        </div>
    </div>
    <div class="row">
        <a href="{{ route('questoes.questao4')}}" class="btn green waves-effect waves-light left">Anterior</a>
        <a href="{{ route('questoes.questao6')}}" class="btn green waves-effect waves-light right">Próxima</a>
    </div>
</div>   
@endsection
