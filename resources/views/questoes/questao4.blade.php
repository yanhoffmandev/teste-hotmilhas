@extends('app')

@section('content')
<div class="container">
    <h2 class="center">Questão 4</h2>
    <div class="row">
        <nav>
            <div class="nav-wrapper orange">
                <div class="col s12">
                <a href="{{route('home')}}" class="breadcrumb breadhover">Início</a>
                    <a class="breadcrumb">Questão 4</a>
                </div>
            </div>
        </nav>
    </div>

    <div class="row">
        <h5 id="ti4" class="titulo">
            Qual o resultado do trecho de código a seguir:
            <p><small>&lt;?php</small></p>
            <p><small>class W {</small></p>
            <p><small>const XX = 12 + 30;</small></p>
            <p><small>}</small></p>
            <p><small>echo W::XX;</small></p>       
            
            
        </h5>
        <div class="row">
            <div id="resposta4">
                <h4>Resposta</h4>
                <p>O resultado será a soma 42. Nesse caso como é uma constante não precisa instanciar a classe para pegar o valor da constante, tem o mesmo comportamento de função estática.</p>

            </div>
        </div>
    </div>
    
    <div class="row">
        <a href="{{ route('questoes.questao3')}}" class="btn green waves-effect waves-light left">Anterior</a>
        <a href="{{ route('questoes.questao5')}}" class="btn green waves-effect waves-light right">Próxima</a>
    </div>
</div>  
@endsection
