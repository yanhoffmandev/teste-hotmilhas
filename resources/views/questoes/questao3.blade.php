@extends('app')

@section('content')
<div class="container">
    <h2 class="center">Questão 3</h2>

    <div class="row">
        <nav>
            <div class="nav-wrapper orange">
                <div class="col s12">
                <a href="{{route('home')}}" class="breadcrumb breadhover">Início</a>
                    <a class="breadcrumb">Questão 3</a>
                </div>
            </div>
        </nav>
    </div>

    <div class="row">
        <h5 class="titulo">
            Em PHP, escreva as classes necessárias para representar um estacionamento.
        </h5>
        <div class="row">

            <div id="resposta3">
                <h4>Resposta</h4>
                <blockquote>Foi desenvolvido um estacionamento simples. Contêm três classes estacionamento, veículo e cliente.O estacionamento é rotativo apenas cobra por hora.</blockquote>
                <blockquote>Está localizado na pasta app\Estacionamento.</blockquote> 
                <blockquote>O exemplo abaixo está instaciado no Controller: questoesController@questao3.</blockquote>

                <div class="row">
                    <div class="col s12 m4">
                        <div class="card blue-grey darken-1">
                            <div class="card-content white-text">
                                <span class="card-title">Cliente</span>
                                <p>Id: {{ $estac['cliente']->getId() }}</p>
                                <p>Nome: {{ $estac['cliente']->getNome() }}</p>
                                <p>Cpf: {{ $estac['cliente']->getCpf() }}</p>
                            </div>
                            <div class="card-action white-text">
                                <span>Istanciado pela classe estacionamento.</span><br>
                            </div>
                        </div>
                    </div>
                
            
                    <div class="col s12 m4">
                        <div class="card blue-grey darken-1">
                            <div class="card-content white-text">
                                <span class="card-title">Veículo do {{ $estac['cliente']->getNome() }}</span>
                                <p>Modelo: {{ $estac['veiculo']->getModelo() }}</p>
                                <p>Placa: {{ $estac['veiculo']->getPlaca() }}</p>
                                <p>Dono: {{ $estac['cliente']->getNome() }}</p>
                            </div>
                            <div class="card-action white-text">
                                <span>Istanciado pela classe estacionamento.</span><br>
                            </div>
                        </div>
                    </div>

                    <div class="col s12 m4">
                        <div class="card blue-grey darken-1">
                            <div class="card-content white-text">
                                <span class="card-title">Estacionamento</span>
                                <p>Entrada: {{ $estac['estacionamento']->getHoraEntrada() }}</p>
                                <p>Saída: {{ $estac['estacionamento']->getHoraSaida() }}</p>
                                <p>Total: R$ {{ number_format($estac['estacionamento']->totalPagar() ,2,',','.') }}</p>
                            </div>
                            <div class="card-action white-text">
                                <span>O minuto é cobrado como se fosse uma hora.</span><br>
                                <span>Ex.: 7:01 = 8 horas</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <a href="{{ route('questoes.questao2')}}" class="btn green waves-effect waves-light left">Anterior</a>
        <a href="{{ route('questoes.questao4')}}" class="btn green waves-effect waves-light right">Próxima</a>
    </div>
</div>       
@endsection
