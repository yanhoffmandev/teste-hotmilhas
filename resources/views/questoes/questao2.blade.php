@extends('app')

@section('content')
<div class="container">
    <h2 class="center">Questão 2</h2>

    <div class="row">
        <nav>
            <div class="nav-wrapper orange">
                <div class="col s12">
                <a href="{{route('home')}}" class="breadcrumb breadhover">Início</a>
                    <a class="breadcrumb">Questão 2</a>
                </div>
            </div>
        </nav>
    </div>

    <div class="row">
        <h5 class="titulo">
            Em Javascript, qual a diferença entre == e === ?
        </h5>
        <div class="row">

            <div id="resposta2">
                <h4>Resposta</h4>
                <p>O operador (==) compara o valor e faz as conversões de tipos necessárias.</p>
                <blockquote>false == '0' \\ true </blockquote>

                O operador (===) compara o valor e <span id="nao">não</span> faz as conversões de tipo, ou seja, se não for do mesmo tipo a comparação será falsa.
                <blockquote>false === '0' \\ false </blockquote>
            </div>
        </div>
    </div>
    
    <div class="row">
        <a href="{{ route('questoes.questao1')}}" class="btn green waves-effect waves-light left">Anterior</a>
        <a href="{{ route('questoes.questao3')}}" class="btn green waves-effect waves-light right">Próxima</a>
    </div>
</div>  
@endsection
