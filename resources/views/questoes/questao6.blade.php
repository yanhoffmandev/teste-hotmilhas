@extends('app')

@section('content')
<div class="container">
    <h2 class="center">Questão 6</h2>

    <div class="row">
        <nav>
            <div class="nav-wrapper orange">
                <div class="col s12">
                <a href="{{route('home')}}" class="breadcrumb breadhover">Início</a>
                    <a class="breadcrumb">Questão 6</a>
                </div>
            </div>
        </nav>
    </div>

    <div class="row">
        <h5 class="titulo">
            Em sudoku, o objetivo é preencher uma grade 9x9 subdivida em quadrantes 3x3
            com números de 1 a 9 de tal forma que não hajam números repetidos em uma
            mesma coluna, linha ou quadrante. Escreva um procedimento que gere uma matriz
            9x9 válida como resultado de sudoku considerando uma grade vazia.
        </h5>

        <div class="row">
            <form id="form_6"  method="POST" action="{{ route('questoes.resposta6')}}">
                {{ csrf_field() }}
                <button type="submit" id="btn"class="btn blue waves-effect waves-light">Gerar matriz</button>
            </form>
        </div>

        <div id="resposta6">
            <h4>Resposta</h4>
            @if(isset($m))
                <table id="sudoku">
                    <tbody>
                        @for ($l = 0; $l <= 8; $l++)
                            <tr>
                                @for ($c = 0; $c <= 8; $c++)
                                    
                                        <td>{{$m[$l][$c]}}</td>
                                                        
                                @endfor
                            </tr> 
                        @endfor
                    </tbody>
                </table>
            @endif
        </div>
    </div>
    <div class="row">
        <a href="{{ route('questoes.questao5')}}" class="btn green waves-effect waves-light left">Anterior</a>
        <a href="{{ route('questoes.questao7')}}" class="btn green waves-effect waves-light right">Próxima</a>
    </div>
</div>   
@endsection
