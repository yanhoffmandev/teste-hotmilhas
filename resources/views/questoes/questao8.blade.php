@extends('app')

@section('content')
<div class="container">
    <h2 class="center">Questão 8</h2>
    <div class="row">
        <nav>
            <div class="nav-wrapper orange">
                <div class="col s12">
                <a href="{{route('home')}}" class="breadcrumb breadhover">Início</a>
                    <a class="breadcrumb">Questão 8</a>
                </div>
            </div>
        </nav>
    </div>

    <div class="row">
        <h5 class="titulo">
            Você já conhecia algum dos problemas apresentados nesta avaliação? De onde?
        </h5>
        <div class="row">

            <div id="resposta8">
                <h4>Resposta</h4>
                <p> Apenas equação do grau. Da faculdade foi um dos meus primeiros algoritmos.</p>
        
            </div>
        </div>
    </div>
    <div class="row">
        <a href="{{ route('questoes.questao7')}}" class="btn green waves-effect waves-light left">Anterior</a>
    </div>
</div>   
@endsection
