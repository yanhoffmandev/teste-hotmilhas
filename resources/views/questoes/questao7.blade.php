@extends('app')

@section('content')
<div class="container">
    <h2 class="center">Questão 7</h2>
    <div class="row">
        <nav>
            <div class="nav-wrapper orange">
                <div class="col s12">
                <a href="{{route('home')}}" class="breadcrumb breadhover">Início</a>
                    <a class="breadcrumb">Questão 7</a>
                </div>
            </div>
        </nav>
    </div>

    <div class="row">
        <h5 id="ti7" class="titulo">
            Escreva um sistema de enquetes que atenda aos seguintes requisitos:
            <br><small>7.1. Cada enquete consiste de apenas uma pergunta de múltipla escolha </small>
            <br><small>7.2. É possível ter mais de uma enquete ativa ao mesmo tempo</small>
            <br><small>7.3. Deve ser possível saber qual usuário criou cada enquete </small>
            <br><small>7.4. Todas as enquetes são públicas</small>
        </h5>
        <div class="row">
            <div id="resposta7">
                <h4>Resposta</h4>
                @if(Auth::guest())
                    <div class="row">
                        <a href="{{ route('admin.login') }}" class="btn blue waves-effect waves-light">Adicionar Enquete</a>
                    </div>
                @endif
                <div class="row">               
                    @foreach ($enquetes as $e)                       
                        <div class="col s12 m4">
                            <div class="card">
                                <div class="card-title center">
                                    
                                    {{$e->pergunta}}
                                    
                                </div>
                                <div class="divider"></div>
                                <div class="card-content">
                                
                                <form action="{{  route('questoes.voto')  }}" method="POST">
                                    {{ csrf_field() }}
                                        @foreach ($e->options as $key => $o)
                                            <div class="input_field">
                                            <input type="radio" name="voto" value="{{$o->id}}" id="{{$o->id}}" {{ ($key === 0) ? 'checked' : '' }} >
                                            <label for="{{$o->id}}"> {{ $o->nome }} </label>
                                            </div>
                                        @endforeach
                                        <button id="btn-enquete" class="btn-large blue waves-effect waves-light">Votar</button>
                                    </form>

                            
                                    
                                </div>
                                <div class="divider"></div>
                                <div class="card-action">
                                    Por {{ $e->user->name }}
                                </div>
                            </div>
                        </div>
                    @endforeach 
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <a href="{{ route('questoes.questao6')}}" class="btn green waves-effect waves-light left">Anterior</a>
        <a href="{{ route('questoes.questao8')}}" class="btn green waves-effect waves-light right">Próxima</a>
    </div>
</div>   
@endsection
