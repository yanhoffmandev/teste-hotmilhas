@extends('app')

@section('content')
<div class="container">
    <h2 class="center">Questão 1</h2>

    <div class="row">
        <nav>
            <div class="nav-wrapper orange">
                <div class="col s12">
                <a href="{{route('home')}}" class="breadcrumb breadhover">Início</a>
                    <a class="breadcrumb">Questão 1</a>
                </div>
            </div>
        </nav>
    </div>

    <div class="row">
        <h5 class="titulo">
            Escreva uma função que receba como parâmetros os coeficientes de uma equação
            de segundo grau e retorne suas raízes.
        </h5>
        <div class="row">
        <form id="form_1"  method="POST" action="{{ route('questoes.resposta1')}}">
                {{ csrf_field() }}
                @include('layouts.forms._form1')
                <div class="row">
                    <button type="submit" id="btn"class="btn blue waves-effect waves-light">Resolver</button>
                </div>
            </form>
        </div>

        <div id="resposta1">
        </div>
    </div>
    <div class="row right">
    <a href="{{ route('questoes.questao2')}}" class="btn green waves-effect waves-light">Próxima</a>
    </div>
</div>   
@endsection
