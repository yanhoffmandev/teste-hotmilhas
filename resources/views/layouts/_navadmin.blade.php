<nav>
    <div class="nav-wrapper indigo">
        <div class="container">
            <a href="{{ route('admin.usuarios') }}" class="brand-logo">Olá {{ Auth::user()->name }}</a>
            <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
            <ul class="right hide-on-med-and-down">
                    <li><a href="{{ route('home') }}">Questões</a></li>           
                    <li><a href="{{ route('admin.usuarios') }}">Usuários</a></li>
                    <li><a href="{{ route('admin.enquetes') }}">Enquetes</a></li>
                    <li><a href="{{ route('admin.login.sair') }}">Sair</a></li>
                
            </ul>
            <ul class="side-nav" id="mobile-demo">
                    <li><a target="_blank" href="{{ route('home') }}">Questões</a></li>                        
                    <li><a href="{{ route('admin.usuarios') }}">Usuários</a></li>
                    <li><a href="{{ route('admin.enquetes') }}">Enquetes</a></li>
                    <li><a href="{{ route('admin.login.sair') }}">Sair</a></li>
                
            
            </ul>
        </div>    
    </div>
</nav>

