<nav>
    <div class="nav-wrapper indigo darken-2">
        <div class="container">
            <a href="{{ route('home') }}" class="brand-logo">Teste Hotmilhas</a>
            <ul class="right hide-on-med-and-down">
                    <li><a href="{{ route('home') }}">Questões</a></li>                           
            </ul>
            <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
        </div>    
    </div>
</nav>