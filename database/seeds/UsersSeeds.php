<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersSeeds extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!User::where('email', '=', 'admin@mail.com')->count()) {
            $usuario = new User();
            $usuario->name = "Admin";
            $usuario->email = "admin@mail.com";
            $usuario->password = bcrypt("123456");
            $usuario->save();
        } else {
            $usuario = User::where('email', '=', 'admin@mail.com')->first();
            $usuario->name = "Admin";
            $usuario->email = "admin@gmail.com";
            $usuario->password = bcrypt("123456");
            $usuario->update();
        }

    }
}
