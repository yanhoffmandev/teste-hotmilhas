# Teste Hotmilhas

Teste realizado no Laravel. 

Para ver as repostas deverá baixar o repositório:

- git clone https://yanhoffmandev@bitbucket.org/yanhoffmandev/teste-hotmilhas.git

- criar .env:

    DB_CONNECTION=sqlite

    DB_DATABASE=homestead //comentar

- terminal: 
    
    composer update

    php artisan key:generate


Eu criei um sistema navegável para resolver o teste:

Questão 1)  - QuestoesController@resposta1 e Ajax - script.js na pasta public/js

Questão 2)  - View - questao2.blade.php

Questão 3)  - QuestoesController@questao3 
            - Foi criado dentro da pasta app/Estacionamento a classes da respectiva questão.

Questão 4)  - View - questao4.blade.php

Questão 5)  - View - questao5.blade.php

Questão 6)  - QuestoesController@resposta6

Questão 7)  - QuestoesController@questao7
            - Foi criado um botão na página da questão 7 para criar enquetes, mas primeiro deve-se logar para criar, depois de criado e adicionado as opções ele aparecerá
            automaticamente na lista das enquetes na view - questao7

Questão 8)  - View - questao8.blade.php 



